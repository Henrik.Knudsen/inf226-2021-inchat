package inf226.inchat;

import com.lambdaworks.crypto.SCryptUtil;
import inf226.storage.*;
import inf226.util.Maybe;
import inf226.util.Util;

import java.rmi.NoSuchObjectException;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.UUID;
import java.time.Instant;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import inf226.util.immutable.List;

/**
 * This class models the chat logic.
 *
 * It provides an abstract interface to
 * usual chat server actions.
 *
 **/

public class InChat {
    private final Connection connection;
    private final UserStorage userStore;
    private final ChannelStorage channelStore;
    private final EventStorage   eventStore;
    private final AccountStorage accountStore;
    private final SessionStorage sessionStore;
    private final Map<UUID,List<Consumer<Channel.Event>>> eventCallbacks
        = new TreeMap<UUID,List<Consumer<Channel.Event>>>();
    private final HashMap<String, HashMap<String, RoleTitles>> channelRoles;


    public InChat(UserStorage userStore,
                  ChannelStorage channelStore,
                  AccountStorage accountStore,
                  SessionStorage sessionStore,
                  Connection     connection) {
        this.userStore=userStore;
        this.channelStore=channelStore;
        this.eventStore=channelStore.eventStore;
        this.accountStore=accountStore;
        this.sessionStore=sessionStore;
        this.connection=connection;
        this.channelRoles = new HashMap<String, HashMap<String, RoleTitles>>();

    }

    /**
     * An atomic operation in Inchat.
     * An operation has a function run(), which returns its
     * result through a consumer.
     */
    @FunctionalInterface
    private interface Operation<T,E extends Throwable> {
        void run(final Consumer<T> result) throws E,DeletedException;
    }
    /**
     * Execute an operation atomically in SQL.
     * Wrapper method for commit() and rollback().
     */
    private<T> Maybe<T> atomic(Operation<T,SQLException> op) {
        synchronized (connection) {
            try {
                Maybe.Builder<T> result = Maybe.builder();
                op.run(result);
                connection.commit();
                return result.getMaybe();
            }catch (SQLException e) {
                System.err.println(e.toString());
            }catch (DeletedException e) {
                System.err.println(e.toString());
            }
            try {
                connection.rollback();
            } catch (SQLException e) {
                System.err.println(e.toString());
            }
            return Maybe.nothing();
        }
    }

    /**
     * Log in a user to the chat.
     */
    public Maybe<Stored<Session>> login(final String username,
                                        final String password) {
        return atomic(result -> {
            final Stored<Account> account = accountStore.lookup(username);
            final Stored<Session> session =
                sessionStore.save(new Session(account, Instant.now().plusSeconds(60*60*24)));
            // Check that password is not incorrect and not too long.

            if (!(!SCryptUtil.check(password, account.value.password.getPassword()) && !(password.length() > 1000))) {
                result.accept(session);
            }
        });
    }
    
    /**
     * Register a new user.
     */
    public Maybe<Stored<Session>> register(final String username,
                                           final String password) {
        int N = 16384;
        int r = 8;
        int p = 1;

        Password temp = new Password(password);
        boolean checkPass = temp.isGood(password);
        try {
            if (!userStore.lookup(username).equals(Maybe.nothing())) {  // if there is a similar username
                System.err.println("[  ERROR  ] A user with the username " + username + " already exists!");
                return Maybe.nothing();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(checkPass) {
            final String hashed = SCryptUtil.scrypt(temp.getPassword(), N, r, p);

            return atomic(result -> {
                final Stored<User> user =
                    userStore.save(User.create(username));
                final Stored<Account> account =
                    accountStore.save(Account.create(user, hashed));
                final Stored<Session> session =
                    sessionStore.save(new Session(account, Instant.now().plusSeconds(60*60*24)));
                result.accept(session);
                System.out.println("[  INFO  ] User " + username + " successfully registered.");
            });
        }
        System.out.println("[  INFO  ] User " + username + " did not registered.");

        return Maybe.nothing();
    }
    
    /**
     * Restore a previous session.
     */
    public Maybe<Stored<Session>> restoreSession(UUID sessionId) {
        return atomic(result ->
            result.accept(sessionStore.get(sessionId))
        );
    }
    
    /**
     * Log out and invalidate the session.
     */
    public void logout(Stored<Session> session) {
        atomic(result ->
            Util.deleteSingle(session,sessionStore));
    }
    
    /**
     * Create a new channel.
     */
    public boolean nameCheck(String name){
        Pattern pattern = Pattern.compile("[~#@*+%{}<>?\\[\\]|\"\\_^]");
        Matcher matcher = pattern.matcher(name);
        return !(matcher.find());
    }
    public Maybe<Stored<Channel>> createChannel(Stored<Account> account, String name) {
        return atomic(result -> {
            try {
                if (nameCheck(name)){
                    Stored<Channel> channel = channelStore.save(new Channel(name, List.empty()));
                    addRoleToChannel(channel.value, account.value.user.value, RoleTitles.OWNER);
                    hasOwner(channel.value);
                    joinChannel(account, channel.identity);
                    result.accept(channel);
                }
                else{
                    System.err.println("[ ERROR ] Illegal characters in channel name");
                }
            } catch (NoSuchObjectException e) {
                System.err.println("[ ERROR ] No owners for channel.");
            }
        });
    }

    /**
     * Join a channel.
     */
    public Maybe<Stored<Channel>> joinChannel(Stored<Account> account, UUID channelID) {
        return atomic(result -> {
            Stored<Channel> channel = channelStore.get(channelID);
            if (!isBANNED(channel.value, account.value.user.value)) {
            Util.updateSingle(account,
                              accountStore,
                              a -> a.value.joinChannel(channel.value.name,channel));
            Stored<Channel.Event> joinEvent
                = channelStore.eventStore.save(
                    Channel.Event.createJoinEvent(channelID,
                        Instant.now(),
                        account.value.user.value.name.getName()));
            result.accept(
                Util.updateSingle(channel,
                                  channelStore,
                                  c -> c.value.postEvent(joinEvent)));

                if(!hasRole(channel.value, account.value.user.value)) {
                    addRoleToChannel(channel.value, account.value.user.value, RoleTitles.PARTICIPANT);
                }
            System.out.println("[  INFO  ] User " + account.value.user.value.name.getName() + " is a participant");
            } else {
                System.err.println("[ ERROR ] User can't join channel because of ban.");

            }

        });
    }
    
    /**
     * Post a message to a channel.
     */
    public Maybe<Stored<Channel>> postMessage(Stored<Account> account, Stored<Channel> channel, String message) {
        return atomic(result -> {
            if(canPost(channel.value ,account.value.user.value)) {
                Stored<Channel.Event> event
                        = channelStore.eventStore.save(
                        Channel.Event.createMessageEvent(channel.identity, Instant.now(),
                                account.value.user.value.name.getName(), message));
                result.accept(
                        Util.updateSingle(channel,
                                channelStore,
                                c -> c.value.postEvent(event)));
            }
            System.out.println("[  INFO  ] User " + account.value.user.value.name.getName() + " cannot post.");
        });
    }
    
    /**
     * A blocking call which returns the next state of the channel.
     */
    public Maybe<Stored<Channel>> waitNextChannelVersion(UUID identity, UUID version) {
        try{
            return Maybe.just(channelStore.waitNextVersion(identity, version));
        } catch (DeletedException e) {
            return Maybe.nothing();
        } catch (SQLException e) {
            return Maybe.nothing();
        }
    }
    
    /**
     * Get an event by its identity.
     */
    public Maybe<Stored<Channel.Event>> getEvent(UUID eventID) {
        return atomic(result ->
            result.accept(channelStore.eventStore.get(eventID))
        );
    }
    
    /**
     * Delete an event.
     */
    public Stored<Channel> deleteEvent(Stored<Account> account, Stored<Channel> channel, Stored<Channel.Event> event) {
        return this.<Stored<Channel>>atomic(result -> {
            User user = userFromName(event.value.sender);
            if(canEditDeleteAny(channel.value, user)) {
                Util.deleteSingle(event , channelStore.eventStore);
                result.accept(channelStore.noChangeUpdate(channel.identity));
                System.out.println("[  INFO  ] User " + account.value.user.value.name.getName() + " deleted post.");
            }
            else {
                System.out.println("[  INFO  ] User " + account.value.user.value.name.getName() + " cannot deleted post.");

            }
        }).defaultValue(channel);

    }

    /**
     * Edit a message.
     */
    public Stored<Channel> editMessage(Stored<Account> account, Stored<Channel> channel, Stored<Channel.Event> event, String newMessage) {
        return this.<Stored<Channel>>atomic(result -> {
            User user = userFromName(event.value.sender);
            if(canEditDeleteAny(channel.value, user)) {
                Util.updateSingle(event,
                        channelStore.eventStore,
                        e -> e.value.setMessage(newMessage));
                result.accept(channelStore.noChangeUpdate(channel.identity));
                System.out.println("[  INFO  ] User " + account.value.user.value.name.getName() + " edited post.");
            }
            else {
                System.out.println("[  INFO  ] User " + account.value.user.value.name.getName() + " cannot edit post.");
            }
        }).defaultValue(channel);
    }
    /**
     * Creating role checks.
     *
     * Owner: Can set roles, delete and edit any message, as well as read and post to the channel.
     * Moderator: Can delete and edit any message, as well as read and post to the channel.
     * Participant: Can delete and edit their own messages, as well as read and post to the channel.
     * Observer: Can read messages in the channel.
     * Banned: Has no access to the channel.
     */

    private boolean canPost(Channel channel, User user) {
        RoleTitles role = channelRoles.get(channel.name).get(user.name.getName());
        return role == RoleTitles.OWNER || role == RoleTitles.MODERATOR || role == RoleTitles.PARTICIPANT;

    }
    private boolean canEditDeleteAny(Channel channel, User user) {
        RoleTitles role = channelRoles.get(channel.name).get(user.name.getName());
        return role == RoleTitles.OWNER || role == RoleTitles.MODERATOR;

    }
    /**
     * ownerOfMessage does not work properly as it does not get the correct information about the origin of the post
     */
    private boolean canEditDeleteOwn(Channel channel, User user, User ownerOfMessage) {
        return (isPARTICIPANT(channel, user) || user.equals(ownerOfMessage));
    }
    private boolean isBANNED(Channel channel, User user) {
        RoleTitles role = channelRoles.get(channel.name).get(user.name.getName());
        return role == RoleTitles.BANNED;
    }

    public boolean isOWNER(Channel channel, User user) {
        RoleTitles role = channelRoles.get(channel.name).get(user.name.getName());
        return role == RoleTitles.OWNER;
    }

    private boolean isPARTICIPANT(Channel channel, User user) {
        RoleTitles role = channelRoles.get(channel.name).get(user.name.getName());
        return role == RoleTitles.PARTICIPANT;
    }
    private boolean hasRole(Channel channel, User user) {
        RoleTitles role = channelRoles.get(channel.name).get(user.name.getName());
        return role != null;
    }

    /**
     * adds role to channel
     */

    public void addRoleToChannel(Channel channelName, User user, RoleTitles role) {
        if(!channelRoles.containsKey(channelName.name)) {
            HashMap<String, RoleTitles> innerHash = new HashMap<>();
            innerHash.put(user.name.getName(),role);
            channelRoles.put(channelName.name, innerHash);
        } else {
            channelRoles.get(channelName.name).put(user.name.getName(), role);
        }
    }
    /**
     * finding user from input string
     */

    public User userFromName(String username) {
        try {
            return userStore.lookup(username).get().value;
        } catch (Maybe.NothingException e) {
            System.err.println("[ ERROR ] Could not find user with name \"" + username + "\".");
        }
        return null;
    }
    /**
     * Checks if owner exists
     */

    private void hasOwner(Channel channel) throws NoSuchObjectException {
        HashMap<String, RoleTitles> userRoles = channelRoles.get(channel.name);

        for (RoleTitles r : userRoles.values()) {
            if (r == RoleTitles.OWNER)
                return;
        }

        throw new NoSuchObjectException("[ ERROR ] Channel has no owner.");
    }





}


