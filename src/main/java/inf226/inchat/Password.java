package inf226.inchat;


public final class Password {

    private final String password;

    public static Password create(String password) {
        return new Password(password);
    }

    public String getPassword() {
        return password;
    }

    public Password(String password) {
        this.password = password;
    }

    public boolean isGood(String password) {
        if(isLength(password) && isRepetitive(password) && !isCommonOrRelatedOrFromBreaches(password))
            return true;
        return false;
    }

    private boolean isLength(String password) {
        return password.length() >= 8 && password.length() <= 64;
    }

    private boolean isRepetitive(String password) {
        char[] chars = password.toCharArray();

        for(int i=0; i<chars.length-2; i++){
            if((chars[i] == chars[i+1]) && chars[i+1] ==chars[i+2]) {
                return false;
            }
            if(chars[i+1] == chars[i] + 1 && chars[i+2] == chars[i+1]) {
                return false;
            }
        }
        return true;
    }

    private boolean isCommonOrRelatedOrFromBreaches (String password) {
        String[] passwordList = new String[]{"123456", "123456789", "qwerty", "password", "12345", "qwerty123", "1q2w3e", "12345678", "111111", "1234567890"};
        for (String s: passwordList) {
            if(s.equals(password))
                return false;
        }
        return false;
    }

}
