package inf226.inchat;

public class Role {
    public static RoleTitles stringToRole(String role) {
        switch (role) {
            case "owner":
                return RoleTitles.OWNER;
            case "moderator":
                return RoleTitles.MODERATOR;

            case "participant":
                return RoleTitles.PARTICIPANT;

            case "observer":
                return RoleTitles.OBSERVER;

            case "banned":
                return RoleTitles.BANNED;

            default:
                return RoleTitles.REGISTERED_ONLY;
        }
    }
}
