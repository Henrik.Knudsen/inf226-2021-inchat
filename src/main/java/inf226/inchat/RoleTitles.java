package inf226.inchat;

public enum RoleTitles {
    OWNER,
    MODERATOR,
    PARTICIPANT,
    OBSERVER,
    BANNED,
    REGISTERED_ONLY,
}
