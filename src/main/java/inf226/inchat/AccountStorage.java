package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;

import inf226.util.immutable.List;
import inf226.util.*;

/**
 * This class stores accounts in the database.
 */
public final class AccountStorage
    implements Storage<Account,SQLException> {
    
    final Connection connection;
    final Storage<User,SQLException> userStore;
    final Storage<Channel,SQLException> channelStore;
   
    /**
     * Create a new account storage.
     *
     * @param  connection   The connection to the SQL database.
     * @param  userStore    The storage for User data.
     * @param  channelStore The storage for channels.
     */
    public AccountStorage(Connection connection,
                          Storage<User,SQLException> userStore,
                          Storage<Channel,SQLException> channelStore) 
      throws SQLException {
        this.connection = connection;
        this.userStore = userStore;
        this.channelStore = channelStore;
        
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Account (id TEXT PRIMARY KEY, version TEXT, user TEXT, password TEXT, FOREIGN KEY(user) REFERENCES User(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS AccountChannel (account TEXT, channel TEXT, alias TEXT, ordinal INTEGER, PRIMARY KEY(account,channel), FOREIGN KEY(account) REFERENCES Account(id) ON DELETE CASCADE, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Account> save(Account account)
      throws SQLException {
        
        final Stored<Account> stored = new Stored<Account>(account);
        String sql =
                "INSERT INTO Account VALUES('" + stored.identity + "','"
                        + stored.version  + "','"
                        + account.user.identity + "','"
                        + account.password.getPassword() + "')";
        connection.createStatement().executeUpdate(sql);
        
        // Write the list of channels
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;
            try {
                final PreparedStatement msql = connection.prepareStatement( "INSERT INTO AccountChannel VALUES('" + stored.identity + "','"
                    + channel.identity + "','"
                    + alias + "', ?,'"
                    + ordinal.get().toString() + "')");
                msql.setString(1 ,alias);
                msql.executeUpdate();
            }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
        return stored;
    }
    
    @Override
    public synchronized Stored<Account> update(Stored<Account> account,
                                            Account new_account)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Account> current = get(account.identity);
    final Stored<Account> updated = current.newVersion(new_account);
    if(current.version.equals(account.version)) {
        String sql = "UPDATE Account SET" +
            " (version,user) =('" 
                            + updated.version  + "','"
                            + new_account.user.identity
                            + "') WHERE id='"+ updated.identity + "'";
        connection.createStatement().executeUpdate(sql);
        
        
        // Rewrite the list of channels
        connection.createStatement().executeUpdate("DELETE FROM AccountChannel WHERE account='" + account.identity + "'");
        
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        new_account.channels.forEach(element -> {
            String alias = element.first;
            Stored<Channel> channel = element.second;

            try {
                final PreparedStatement msql = connection.prepareStatement("INSERT INTO AccountChannel VALUES('" + account.identity + "','"
                        + channel.identity + "', ?,'"
                        + ordinal.get().toString() + "')");
                msql.setString(1, alias);
                msql.executeUpdate();
            }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
    } else {
        throw new UpdatedException(current);
    }
    return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Account> account)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Account> current = get(account.identity);
        if(current.version.equals(account.version)) {
        String sql =  "DELETE FROM Account WHERE id ='" + account.identity + "'";
        connection.createStatement().executeUpdate(sql);
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Account> get(UUID id)
      throws DeletedException,
             SQLException {

        final String accountsql = "SELECT version,user,password FROM Account WHERE id = ?";
        final String channelsql = "SELECT channel,alias,ordinal FROM AccountChannel WHERE account = ? ORDER BY ordinal DESC";

        final PreparedStatement accountStatement = connection.prepareStatement(accountsql);
        final PreparedStatement channelStatement = connection.prepareStatement(channelsql);

        accountStatement.setString(1,id.toString());
        channelStatement.setString(1,id.toString());

        final ResultSet accountResult = accountStatement.executeQuery();
        final ResultSet channelResult = channelStatement.executeQuery();

        if(accountResult.next()) {
            final UUID version = UUID.fromString(accountResult.getString("version"));
            final UUID userid =
            UUID.fromString(accountResult.getString("user"));
            final String password =
            accountResult.getString("password");
            final Stored<User> user = userStore.get(userid);
            // Get all the channels associated with this account
            final List.Builder<Pair<String,Stored<Channel>>> channels = List.builder();
            while(channelResult.next()) {
                final UUID channelId = 
                    UUID.fromString(channelResult.getString("channel"));
                final String alias = channelResult.getString("alias");
                channels.accept(
                    new Pair<String,Stored<Channel>>(
                        alias,channelStore.get(channelId)));
            }
            return (new Stored<Account>(new Account(user,channels.getList(),new Password(password)),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up an account based on their username.
     */
    public Stored<Account> lookup(String username)
      throws DeletedException,
             SQLException {
        final String sql = "SELECT Account.id from Account INNER JOIN User ON user=User.id where User.name= ?";
        final PreparedStatement statement = connection.prepareStatement(sql);

        statement.setString(1, username);
        final ResultSet rs = statement.executeQuery();
        System.err.println(sql);

        if(rs.next()) {
            final UUID identity = 
                    UUID.fromString(rs.getString("id"));
            return get(identity);
        }
        throw new DeletedException();
    }
    
} 
 
